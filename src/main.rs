use clap::{value_t, App, Arg};
use rayon::prelude::*;
use reqwest::Client;
use select::{document::Document, predicate::Class};

fn client(user_agent: &str, cookie: &str) -> Client {
    use reqwest::header;

    let mut headers = header::HeaderMap::new();
    headers.insert(
        header::USER_AGENT,
        header::HeaderValue::from_str(user_agent).unwrap(),
    );
    headers.insert(
        header::COOKIE,
        header::HeaderValue::from_str(cookie).unwrap(),
    );

    Client::builder().default_headers(headers).build().unwrap()
}

fn get_noms(client: &Client, id: &str, skip: i32) -> Option<Vec<String>> {
    let html: String = client
        .get(&format!("https://www.leetchi.com/en/Fundraising/PartialBlocParticipationsList?fundraisingId={}&skip={}", id, skip))
        .send()
        .ok()?
        .text()
        .ok()?;

    let noms: Vec<String> = Document::from(html.as_str())
        .find(Class("fdr-contributor-name"))
        .filter_map(|e| Some(e.text()))
        .collect();

    if !noms.is_empty() {
        Some(noms)
    } else {
        None
    }
}

fn main() {
    let matches = App::new("noms_leetchi")
        .author("Ealhad")
        .about("Va choper tous les donateurs d’une cagnotte Leetchi.")
        .arg(
            Arg::with_name("USER_AGENT")
                .short("u")
                .long("user-agent")
                .required(true)
                .takes_value(true),
        )
        .arg(
            Arg::with_name("COOKIE")
                .short("c")
                .long("cookie")
                .required(true)
                .takes_value(true),
        )
        .arg(
            Arg::with_name("ID")
                .short("i")
                .long("id")
                .required(true)
                .takes_value(true),
        )
        .arg(
            Arg::with_name("NUMBER")
                .short("n")
                .required(true)
                .takes_value(true),
        )
        .get_matches();

    let client = client(
        matches.value_of("USER_AGENT").unwrap(),
        matches.value_of("COOKIE").unwrap(),
    );

    let id = matches.value_of("ID").unwrap();
    let max = value_t!(matches, "NUMBER", i32).unwrap();

    (0..(max / 24)).into_par_iter().for_each(|n| {
        if let Some(noms) = get_noms(&client, id, n * 24) {
            for nom in noms {
                println!("{}", nom);
            }
        }
    })
}
